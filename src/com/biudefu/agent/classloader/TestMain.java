package com.biudefu.agent.classloader;

import java.io.*;
import java.lang.reflect.InvocationTargetException;

/**
 * @author bjliuyachao
 * @since 2018-08-23 19:49
 */
public class TestMain {

    public static void main(String[] args) throws FileNotFoundException, IOException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
        File file = new File(".");
        System.out.println(file.getCanonicalPath());
        InputStream input = new FileInputStream(file.getCanonicalPath() + "/src/com/biudefu/agent/classloader/Person.class");
        byte[] result = new byte[1024];
        int count = input.read(result);
        CustomClassLoader classLoader = new CustomClassLoader();
        Class clazz = classLoader.defineCustomClass(result, 0, count);
        System.out.println(clazz.getName());
    }

}
