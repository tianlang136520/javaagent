package com.biudefu.agent.javassist;

import javassist.*;

import java.io.*;

/**
 * @author bjliuyachao
 * @since 2018-08-24 10:45
 */
public class TestJavassist {

    private static ClassPool pool = ClassPool.getDefault();

    public static void main(String[] args) throws IOException, NotFoundException, CannotCompileException {

        File file = new File(".");
        System.out.println(file.getCanonicalPath());
        String filename = file.getCanonicalPath() + "/src/com/biudefu/agent/classloader/Person.class";
        String outpath =file.getCanonicalPath() +"/javassis-file/javassis-person.class";

        //通过包名+类名来得到CtClass
        CtClass clazz = pool.get("com.biudefu.agent.classloader.Person");
        CtMethod[] declaredMethods = clazz.getDeclaredMethods();
        for (CtMethod method : declaredMethods){
            String insertCode="System.out.println(\"this is a insert code\");";
            method.insertBefore(insertCode);
        }
        byte[] bytes = clazz.toBytecode();
        writeToNewClass(outpath,bytes);


    }

    private static void writeToNewClass(String outpath, byte[] bytes) throws IOException {
        File file = new File(outpath);
        if (!file.exists()) {   //文件不存在则创建文件，先创建目录
            File dir = new File(file.getParent());
            dir.mkdirs();
            file.createNewFile();
        }
        FileOutputStream outStream = new FileOutputStream(file); //文件输出流将数据写入文件
        outStream.write(bytes);
        outStream.close();
    }

    public CtClass getCtClassByStream(String filename) throws IOException {
        //通过I/O操作指定路径classPath下的字节码文件
        InputStream ins = new FileInputStream(new File(filename));
        return pool.makeClass(ins);
    }

}
