package com.biudefu.agent.client;

import com.biudefu.agent.service.transfer.MyMonitorTimesTransformer;

import java.lang.instrument.Instrumentation;

/**
 * @author bjliuyachao
 * @since 2018-08-13 17:30
 */
public class MyMonitorTimesAgent {

    /**
     * @param agentOps
     * @param inst
     */
    public static void premain(String agentOps, Instrumentation inst) {
        System.out.println("=========MyAgent的premain方法执行========");
        System.out.println(agentOps);
        // 添加Transformer
        inst.addTransformer(new MyMonitorTimesTransformer());
    }

}
