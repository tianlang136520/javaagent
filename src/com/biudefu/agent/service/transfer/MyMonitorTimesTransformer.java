package com.biudefu.agent.service.transfer;


import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.LineNumberAttribute;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author bjliuyachao
 * @since 2018-08-13 16:45
 */
public class MyMonitorTimesTransformer implements ClassFileTransformer {

    final static String prefix = "\nlong startTime = System.currentTimeMillis();\n";
    final static String postfix = "\nlong endTime = System.currentTimeMillis();\n";

    // 被处理的方法列表 <class,list<methods>>
    final static Map<String, List<String>> methodMap = new HashMap<String, List<String>>();

    public MyMonitorTimesTransformer() {
        add("com.biudefu.agent.test.MethodMonitorTimesTest.sayHello");
        add("com.biudefu.agent.test.MethodMonitorTimesTest.sayHello2");
    }

    private void add(String methodString) {
        String className = methodString.substring(0, methodString.lastIndexOf("."));
        String methodName = methodString.substring(methodString.lastIndexOf(".") + 1);
        List<String> list = methodMap.get(className);
        if (list == null) {
            list = new ArrayList<String>();
            methodMap.put(className, list);
        }
        list.add(methodName);
    }

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?>
            classBeingRedefined, ProtectionDomain protectionDomain, byte[]
                                    classfileBuffer) throws IllegalClassFormatException {
        className = className.replace("/", ".");
//        System.out.println("transform classname:"+className);
        if (!methodMap.containsKey(className)) {
            return null;
        }
        try {
            CtClass ctClass = ClassPool.getDefault().get(className);
            for (String agent_method : methodMap.get(className)) {

                CtMethod declaredMethod = ctClass.getDeclaredMethod(agent_method);
                CodeAttribute ca = declaredMethod.getMethodInfo().getCodeAttribute();
                LineNumberAttribute ainfo
                        = (LineNumberAttribute) ca.getAttribute(LineNumberAttribute.tag);
                int length = ainfo.tableLength();
                System.out.println("========methodname:"+agent_method+"========");
                for (int i=0;i<length;i++){
                    int source_number = ainfo.lineNumber(i);
                    System.out.println("tablenumber:"+i+",source_number:"+source_number);
                }
                System.out.println("=========================");
//                System.out.println("agent methodname:"+agent_method+",ainfo-length:"+length);
//                System.out.println("start-pc:"+ainfo.lineNumber(3));
//                LineNumberAttribute.Pc pc = ainfo.toNearPc(10);
//                System.out.println("pc-index:"+pc.index+",pc-line:"+pc.line);
//                int line = pc.line;
//                declaredMethod.insertAt(line, "String str_10 =\"****************\";");
//                declaredMethod.insertAt(line+1, "String str_10 =\"****************\";");


//                ControlFlow cf = new ControlFlow(declaredMethod);
//
//                for (ControlFlow.Block block : cf.basicBlocks()) {
//                    ControlFlow.Catcher catchBlocks[] = block.catchers();
//                    for (int i = 0; i < catchBlocks.length; i++) {
//                        int position = catchBlocks[i].block().position();
//                        // Get source code line position
//                        int lineNumber = declaredMethod.getMethodInfo().getLineNumber(position);
//                        System.out.println
//                                (String.format("position:%s,linenumber:%s.",position,lineNumber));
//                        declaredMethod.insertAt
//                                (lineNumber + 1,
//                                        "System.out.println(\"catch block\");");
//                    }
//                }

//                declaredMethod.insertBefore("long start_time = System.currentTimeMillis();");
//                declaredMethod.insertAt(1, "System.out.println(\"target————1!\");");
//                declaredMethod.insertAt(2, "String str_02 =\"target0020\";");
//                declaredMethod.insertAt(3, "String str_03 =\"target0030\";");
//                declaredMethod.insertAt(4, "String str_04 =\"target0040\";");
//                declaredMethod.insertAt(10, "String str_10 =\"target0100\";");

            }
//            ctClass.writeFile();
            outPutFile(ctClass);
            return ctClass.toBytecode();
        } catch (Exception e) {
            System.out.println("发生异常:" + e.getMessage());
            throw new RuntimeException("java agent execute error!");
        }

    }

//    @Override
//    public byte[] transform(ClassLoader loader, String className, Class<?>
//            classBeingRedefined, ProtectionDomain protectionDomain, byte[]
//                                    classfileBuffer) throws IllegalClassFormatException {
//
//        className = className.replace("/", ".");
//        if (methodMap.containsKey(className)) {// 判断加载的class的包路径是不是需要监控的类
//            CtClass ctclass = null;
//            try {
//                ctclass = ClassPool.getDefault().get(className);// 使用全称,用于取得字节码类<使用javassist>
//                for (String methodName : methodMap.get(className)) {
//                    String outputStr = "\nSystem.out.println(\"this method " + methodName
//                            + " cost:\" +(endTime - startTime) +\"ms.\");";
//
//                    CtMethod ctmethod = ctclass.getDeclaredMethod(methodName);// 得到这方法实例
//                    String newMethodName = methodName + "$old";// 新定义一个方法叫做比如sayHello$old
//                    ctmethod.setName(newMethodName);// 将原来的方法名字修改
//
//                    // 创建新的方法，复制原来的方法，名字为原来的名字
//                    CtMethod newMethod = CtNewMethod.copy(ctmethod, methodName, ctclass, null);
//
//                    // 构建新的方法体
//                    StringBuilder bodyStr = new StringBuilder();
//                    bodyStr.append("{");
//                    bodyStr.append(prefix);
//                    bodyStr.append(newMethodName + "($$);\n");// 调用原有代码，类似于method();($$)表示所有的参数
//                    bodyStr.append(postfix);
//                    bodyStr.append(outputStr);
//                    bodyStr.append("}");
//
//                    newMethod.setBody(bodyStr.toString());// 替换新方法
//                    ctclass.addMethod(newMethod);// 增加新方法
//                }
//                String outpath = new File(".").getCanonicalPath() + "/javassis-file/javassis-person.class";
//                try {
//                    File file = new File(outpath);
//                    if (!file.exists()) {   //文件不存在则创建文件，先创建目录
//                        File dir = new File(file.getParent());
//                        dir.mkdirs();
//                        file.createNewFile();
//                    }
//                    FileOutputStream outStream = new FileOutputStream(file); //文件输出流将数据写入文件
//                    outStream.write(ctclass.toBytecode());
//                    outStream.close();
//                } catch (Exception e) {
//                    throw new RuntimeException("输出发生异常！");
//                } finally {
//                }
//
//                return ctclass.toBytecode();
//            } catch (Exception e) {
//                System.out.println(e.getMessage());
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }

    public static void outPutFile(CtClass ctclass) throws IOException {
        String outpath = new File(".")
                .getCanonicalPath() + "/javassis-file/javassis-person.class";
        try {
            File file = new File(outpath);
            if (!file.exists()) {   //文件不存在则创建文件，先创建目录
                File dir = new File(file.getParent());
                dir.mkdirs();
                file.createNewFile();
            }
            FileOutputStream outStream = new FileOutputStream(file); //文件输出流将数据写入文件
            outStream.write(ctclass.toBytecode());
            outStream.close();
        } catch (Exception e) {
            throw new RuntimeException("输出发生异常！");
        } finally {
        }
    }

}
