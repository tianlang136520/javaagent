package com.biudefu.agent.test;

/**
 * @author bjliuyachao
 * @since 2018-08-13 16:55
 */
public class MethodMonitorTimesTest {

    public static void main(String[] args) throws InterruptedException {
        sayHello();
        sayHello2(Integer.parseInt(args[0]));
    }

    public static void sayHello() {
        try {
            Thread.sleep(2000);
            System.out.println("hello world!!");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void sayHello2(int nums) throws InterruptedException {

        for (int i=0;i<nums;i++){
            System.out.println(i);
        }
//        try {
//            Thread.sleep(1000);
//            System.out.println(hello);
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
    }

}
